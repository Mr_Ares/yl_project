import sys

from PyQt5 import uic
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import (QApplication, QWidget, QListWidget, QStackedWidget,
                             QHBoxLayout, QListWidgetItem, QLabel, QComboBox)
import sqlite3
from modul_serial import *


def get(bd_table: str, bd_col: str):
    con = sqlite3.connect('db.sqlite')
    global cur
    cur = con.cursor()
    info = cur.execute(f'SELECT {bd_col} FROM {bd_table}').fetchall()
    cur.close()
    return info


def except_hook(cls, exception, traceback):
    sys.__excepthook__(cls, exception, traceback)


class MyWidget(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("1.ui", self)
        self.ports_list.addItems(get_serial_ports())
        self.modes = get('modes', 'id, description')
        self.modes_list.addItems([i[1] for i in self.modes])
        self.connect_button.clicked.connect(self.connect)
        self.send_button.clicked.connect(self.send)

    def connect(self):
        print(self.ports_list.currentText())
        try:
            self.back_light = BackLight(self.ports_list.currentText())
            self.connect_button.setText('Подключено')
        except Exception:
            pass

    def send(self):
        self.back_light.send(str(self.modes[self.modes_list.currentIndex()][0]))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyWidget()
    ex.show()
    sys.excepthook = except_hook
    sys.exit(app.exec())
