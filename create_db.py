import sqlite3


def get(bd_table: str, bd_col: str, bd_where: str):
    return cur.execute(f'SELECT {bd_col} FROM {bd_table} WHERE {bd_where}').fetchall()


def insert(bd_table: str, bd_cols: str, values: tuple):
    return cur.execute(f'INSERT INTO {bd_table}{bd_cols} VALUES {values}').fetchall()


def update(bd_table: str, new_info, bd_where: str):
    return cur.execute(f'UPDATE {bd_table} SET {new_info} WHERE {bd_where}').fetchall()


def delete(bd_table: str, bd_where: str = ''):
    if bool(bd_where):
        return cur.execute(f'DELETE from {bd_table} WHERE {bd_where}').fetchall()
    else:
        return cur.execute(f'DELETE from {bd_table}').fetchall()


def main(name: str):
    con = sqlite3.connect(name)
    global cur
    cur = con.cursor()

    # update('films', 'duration = 42', "duration = ''")
    insert('modes', '', values=str(bd_info)[1:-1])

    con.commit()
    con.close()


with open('modes.txt', 'r', encoding='UTF-8') as file:
    file_lines = file.readlines()

bd_info = []

for file_line in file_lines:
    mode_id, description = file_line.split(' - ')
    bd_info.append((int(mode_id), description[:-1]))

print(bd_info)

if __name__ == '__main__':
    main('db.sqlite')
