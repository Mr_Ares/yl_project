import serial
from serial.tools.list_ports import comports
from time import sleep
from contextlib import contextmanager
import threading
import _thread


class TimeoutException(Exception):
    def __init__(self, msg=''):
        self.msg = msg


@contextmanager
def time_limit(seconds, msg=''):
    timer = threading.Timer(seconds, lambda: _thread.interrupt_main())
    timer.start()
    try:
        yield
    except KeyboardInterrupt:
        raise TimeoutException("Timed out for operation {}".format(msg))
    finally:
        # if the action ends in specified time, timer is canceled
        timer.cancel()


class ConnectError(Exception):
    pass


class BackLight:
    def __init__(self, port):
        self.ser = serial.Serial(port=port, baudrate=9600)
        if not self.test_connect():
            raise ConnectError

    def test_connect(self):
        try:
            with time_limit(3, 'Not work!'):
                print(1)
                return self.ser.readline() == b'I am Ares backlight\r\n'
            return False
        except TimeoutException:
            print(1)
            return False

    def send(self, command: str):
        print(command)
        self.ser.write(bytes(command, encoding='UTF_8'))


def get_serial_ports():
    return [i.name for i in comports(include_links=True)]


if __name__ == '__main__':
    b = BackLight('COM3')
    sleep(1)
    b.send('23')
    sleep(10)
